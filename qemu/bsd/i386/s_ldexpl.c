#include <math.h>
#include <errno.h>
#include <sysdep.h>

/* 4.x doesnt have isfinite */
#ifndef isfinite
#define isfinite __isfinitel

union IEEEl2bits {
	long double	e;
	struct {
		unsigned int	manl	:32;
		unsigned int	manh	:32;
		unsigned int	exp	:15;
		unsigned int	sign	:1;
		unsigned int	junk	:16;
	} bits;
};

static int __isfinitel(long double x)
{
	union IEEEl2bits u;

	u.e = x;
	return (u.bits.exp != 32767);
}
#endif

long double __ldexpl(long double x, int expn)
{
  long double res;
  if (!isfinite (x) || x == 0.0L)
    return x;

  __asm__ ("fscale"
  	    : "=t" (res)
	    : "0" (x), "u" ((long double) expn));

  if (!isfinite (res) || res == 0.0L)
    errno = ERANGE;

  return res;
}

weak_alias(__ldexpl,ldexpl)
