#ifndef _QEMU_BSD_SYSDEP_H_
#define _QEMU_BSD_SYSDEP_H_

#include <sys/cdefs.h>

#define HAVE_ELF

#ifdef __ASSEMBLER__
#define ALIGNARG(log2)				1<<log2
#define ASM_TYPE_DIRECTIVE(name,typearg)	.type name,typearg;
#define ASM_SIZE_DIRECTIVE(name)		.size name,.-name;
#define END(x)
#define strong_alias(sym,alias)			.set alias,sym;
#define weak_alias(sym,alias)			.weak alias; .equ alias,sym;
#else
#define strong_alias(sym,alias)			__strong_reference(sym,alias);
#define weak_alias(sym,alias)			__weak_reference(sym,alias);
#endif

#endif
